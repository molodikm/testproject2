package tests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Owner;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.open;

public class Tests {

    @BeforeEach
    public void option() {
        Configuration.timeout = 6000;
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-extensions");

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        Configuration.browserCapabilities = capabilities;

    }

    @Test
    @Owner("MolodikM")
    @Tag("registration")
    @DisplayName("тест на Selenide для mail.ru")
    public void firstSelenide() {
        open("https://account.mail.ru/signup?from=vk&state=%257B%2522query%2522%253A%257B%2522app_id_mytracker%2522%253A%252258519%2522%252C%2522from%2522%253A%2522vk%2522%252C%2522rf%2522%253A%2522auth.mail.ru%2522%257D%252C%2522act_token%2522%253A%25229c09e50f537948ac8d65a68094a1902b%2522%257D");
        $(By.name("fname")).click();
        $(By.name("fname")).setValue("Михаил");

        $(By.name("lname")).click();
        $(By.name("lname")).setValue("Молодик");

        $(By.xpath("//div[@class='page-content']/div[3]/div[3]//form/div[8]//div[@class='styles__container--1KvR8']/label[2]//div[@class='border-0-2-100']")).click();

        $(By.xpath("//input[@id='aaa__input']")).click();
        $(By.xpath("//input[@id='aaa__input']")).setValue("molodikmmm");

        $(By.xpath("//div[@id='aaa__select']/select[@class='select-0-2-177']")).click();
        $(By.name("@inbox.ru")).click();

        $(By.xpath("//input[@id='password']")).click();
        $(By.xpath("//input[@id='password']")).setValue("m12345678mmm");

        $(By.xpath("//div[@class='page-content']/div[3]/div[3]/div//form/button[@type='submit']")).click();

    }
}















        /*$(By.name("q")).setValue("Открытие Википедия").pressEnter();
        ElementsCollection resultSearch = $$(By.xpath("//div[@class='g']"));
        System.out.println(resultSearch);
        SelenideElement elem = resultSearch.find(text("Открытие (банк) - — Википедия"));
        System.out.println("_______");
        System.out.println(elem.getText());
        SelenideElement elemOtkr = $(By.xpath("//div[@class='g']")).shouldHave(text("ПАО Банк «Финансовая корпорация Открытие» (работает под брендом"));
        elemOtkr.$(By.xpath(".//a[@href]")).click();
        switchTo().window(1);
        sleep(5000);
        closeWebDriver();
    }

    @Test
    @Owner("Tolstoy")
    @Tag("two")
    @DisplayName("Проверка курса валют на сайте Открытия")
    public void otkrSelenide(){
        GoogleMainPage googlePage = open("https://www.google.ru/", GoogleMainPage.class);
        GoogleSearchResult googleSearchResult = googlePage.search("Открытие банк");
        OpenMainPage openPage = googleSearchResult.goLinkByName("Банк Открытие: Частным клиентам");
        System.out.println(openPage.getCourse("USD","Банк покупает"));
        Assertions.assertTrue(
                openPage.getCourse("USD","Банк покупает")
                        <
                        openPage.getCourse("USD","Банк продает"),
                "Курс покупки USD не меньше курса продажи"
        );
    }

}
}  */

